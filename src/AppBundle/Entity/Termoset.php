<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Termoset
 *
 * @ORM\Table(name="termoset")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TermosetRepository")
 */
class Termoset
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="termo_date", type="string")
     */
    private $termo_date;

    /**
     * @ORM\Column(name="termo_blob", type="text")
     */
    private $termo_blob;

    /**
     * @ORM\Column(name="number", type="integer")
     */
    private $number;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * Set termoDate
     *
     * @param string $termoDate
     *
     * @return Termoset
     */
    public function setTermoDate($termoDate)
    {
        $this->termo_date = $termoDate;

        return $this;
    }

    /**
     * Get termoDate
     *
     * @return string
     */
    public function getTermoDate()
    {
        return $this->termo_date;
    }

    /**
     * Set termoBlob
     *
     * @param string $termoBlob
     *
     * @return Termoset
     */
    public function setTermoBlob($termoBlob)
    {
        $this->termo_blob = $termoBlob;

        return $this;
    }

    /**
     * Get termoBlob
     *
     * @return string
     */
    public function getTermoBlob()
    {
        return $this->termo_blob;
    }

    /**
     * Set number
     *
     * @param integer $number
     *
     * @return Termoset
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer
     */
    public function getNumber()
    {
        return $this->number;
    }
}
