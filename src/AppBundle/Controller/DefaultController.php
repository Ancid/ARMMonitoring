<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Termoset;
use Doctrine\ORM\Query;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        return $this->render(':default:index.html.twig', [
//            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/termometr/", name="termometr")
     */
    public function termometrAction(Request $request)
    {
        $query = $this->getDoctrine()
            ->getRepository('AppBundle:Termoset')
            ->createQueryBuilder('t')
            ->getQuery();
        $numbers = $query->getResult(Query::HYDRATE_ARRAY);

        return $this->render(':default:termometr.html.twig', [
            'numbers' => $numbers
        ]);
    }

    /**
     * @Route("/termo_graph/", name="termo_graph")
     */
    public function termoGraphAction(Request $request)
    {
        $checked = $request->get('termosets');
        $intChecked = array_map('intval', $checked);
        $sets = $this->getDoctrine()->getRepository('AppBundle:Termoset')->findBy(['id' => $intChecked]);

        return $this->render(':default:termo_graph.html.twig', [
            'sets' => $sets
        ]);
    }

    /**
     * @Route("/tube/", name="tube")
     */
    public function tubeAction(Request $request)
    {
        return $this->render(':default:tube.html.twig', [
        ]);
    }

    /**
     * @Route("/upload/", name="upload")
     */
    public function uploadPageAction(Request $request)
    {
        return $this->render(':default:upload.html.twig', []);
    }

    /**
     * @Route("/file_upload/", name="file_upload")
     */
    public function uploadAction(Request $request)
    {
        if ($request->isXmlHttpRequest() && !$request->isMethod('POST')) {
            throw new \HttpException('XMLHttpRequests/AJAX calls must be POSTed');
        }

        /* @var UploadedFile */
        $file = $request->files->get('xls-file');

        //  Read your Excel workbook
        try {
            $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject($file->getPathname());

            //  Get worksheet dimensions
            $sheet = $phpExcelObject->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();

            $allSheet = [];
            $cur_date = $cur_num = false;
            for ($row = 6; $row <= $highestRow; $row++){
                $i_row = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, TRUE);
                if (null === $i_row[0][2]) {
                    $tset = false;
                    continue;
                }
                if (null !== $i_row[0][0]) {
                    //дата
                    $tmp = explode('-', $i_row[0][1]);
                    $cur_date = $tmp[1].'-'.$tmp[0].'-'.$tmp[2];
                    //номер
                    $num = [];
                    preg_match("/.*\[\s(\d*)\s\]/u", $i_row[0][2], $num);
                    $cur_num = (int)$num[1];
                } else {
                    //сохраняем замеры
                    $blob = [];
                    for ($i = 4; $i < count($i_row[0]); $i++) {
                        if (null === $i_row[0][$i]) {
                            break;
                        }
                        $blob[] = (float)$i_row[0][$i];
                    }
                    $allSheet[$cur_num][$cur_date] = $blob;
                    $cur_date = $cur_num = false;
                }
            }

            foreach ($allSheet as $num => $dates) {
                foreach ($dates as $date => $arr) {
//                    var_dump($arr);
//                    die();
                    $set = new Termoset();
                    $set->setTermoDate($date)
                        ->setNumber($num)
                        ->setTermoBlob(json_encode(array_values($arr)));
                    $this->getDoctrine()->getManager()->persist($set);
                }
            }
            $this->getDoctrine()->getManager()->flush();





        } catch(\Exception $e) {
            die('Error loading file "'.pathinfo($file->getPathname(), PATHINFO_BASENAME).'": '.$e->getMessage());
        }

        return new Response(json_encode($allSheet));
    }
}
